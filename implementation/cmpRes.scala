package project
package implementation

abstract sealed class CmpRes {
    def orElse(other: => CmpRes) = this match {
        case  Eq => other
        case neq => neq
    }

    def inverse = this match {
        case Eq => Eq
        case Lt => Gt
        case Gt => Lt
    }
}

case object Lt extends CmpRes
case object Eq extends CmpRes
case object Gt extends CmpRes

object CmpRes{
    object lemmas{
        /* -------- LEMMAS -------- */
        
        def totalEqualImpliesFirstEqual(a: CmpRes, b: CmpRes) = {
            require((a orElse b) == Eq)
        }.ensuring( a == Eq)

        def totalHasValueDifferentThanFirstImpliesFirstEqual(a: CmpRes, b: CmpRes) = {
            require( (a orElse b) != a )
        }.ensuring( a == Eq )

        def inverseOfInverseIsIdentity(a: CmpRes) = {

        }.ensuring( a == a.inverse.inverse)

        def inverseIsSymetrical(a: CmpRes, b: CmpRes) = {
            require(a == b.inverse)
        }.ensuring(a.inverse == b)
    }
}