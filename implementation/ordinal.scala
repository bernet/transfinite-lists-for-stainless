package project
package implementation

import stainless.annotation._
import stainless.lang._
import stainless.collection._
import stainless.equations._

import CmpRes.lemmas._

abstract sealed class Ordinal{

    def isLimit: Boolean
    def isSucc: Boolean

    def exp: Ordinal

    def length: BigInt = {
        val res: BigInt = this match {
            case Nat(_) => 0
            case Transfinite(_, _, tail) => 1 + tail.length
        }
        res
    }.ensuring( res => res >= 0 )

    def size: BigInt = {
        val res: BigInt = this match {
            case Nat(_) => 1
            case Transfinite(exp, _, tail) => exp.size + tail.size
        }
        res
    }.ensuring( res => res > 0 )


    def cmp(that: Ordinal): CmpRes = Ordinal.cmp(this, that)

    def < (that: Ordinal): Boolean = (this cmp that) == Lt
    override def equals (that: Ordinal): Boolean = (this cmp that) == Eq
    def <= (that: Ordinal): Boolean = (this cmp that) != Gt

    @inlineOnce
    def + (that: Ordinal) = Ordinal.add(this, that)

    @inlineOnce
    def * (that: Ordinal) = Ordinal.mult(this, that)

    def max(that: Ordinal) = Ordinal.max(this, that).ensuring(res => ((this == res) || (that == res)) && this <= res && that <= res)
}

//case object Zero extends Ordinal(isLimit = false)

case class Nat(n: BigInt) extends Ordinal{
    require( 0 <= n)
    override def isLimit: Boolean = false
    override lazy val isSucc: Boolean = 0 < n
    override lazy val exp = Nat(0)
} //could be represented as Transfinite(0, n, <undefined (or zero?)>)

case class Transfinite(exp: Ordinal, coef: BigInt, tail: Ordinal) extends Ordinal{
    require(Nat(0) < exp  && 0 < coef && tail.exp < exp)
    override lazy val isLimit: Boolean = !isSucc
    override lazy val isSucc: Boolean = tail.isSucc

    lazy val omegaExp = Transfinite(exp, 1, Nat(0))

    lazy val head = Transfinite(exp, coef, Nat(0))
}

object Ordinal{
    

    def cmpN(a: BigInt, b: BigInt): CmpRes = if(a < b) Lt else if (b < a) Gt else Eq

    def cmp(alpha: Ordinal, beta: Ordinal): CmpRes = {

        (alpha, beta) match {
            case (Nat(a), Nat(b))   => cmpN(a,b)
            case (Nat(_), _     )   => Lt
            case (_,      Nat(_))   => Gt
            case (Transfinite(exp1, coef1, tail1), 
                  Transfinite(exp2, coef2, tail2)) =>
                    cmp(exp1, exp2) orElse
                    cmpN(coef1, coef2) orElse
                    cmp(tail1, tail2)
        }
    }


    def max(alpha: Ordinal, beta: Ordinal): Ordinal = {
        import lemmas.{Smaller, Equals, Cmp}

        val res = if(alpha < beta){
            Smaller.impliesAtLeastAsSmall(alpha, beta)
            assert(alpha <= beta)
            Equals.impliesAtLeastAsSmall(beta, beta)
            assert(beta <= beta)
            beta
        } else {
            Cmp.symetry(alpha, beta)
            assert(beta <= alpha)
            Equals.impliesAtLeastAsSmall(alpha, alpha)
            assert(alpha <= alpha)
            alpha
        }

        res
    }.ensuring(res => ((alpha == res) || (beta == res)) && alpha <= res && beta <= res )

    def addNat(a: Ordinal, b: Nat): Ordinal = {
        import lemmas.Cmp
        a match {
            case Nat(n1) =>
                val Nat(n2) = b
                Nat(n1 + n2)

            case a@Transfinite(e1, c1, r1) =>

                val nr1 = addNat(r1, b)
                Transfinite(e1, c1, nr1)
        }
    }.ensuring(res => res.exp == a.exp)

    def add(alpha: Ordinal, beta: Ordinal): Ordinal = {
        decreases(alpha.length)
        import lemmas.{Add, Equals, Cmp, Util}

        (alpha, beta) match {
            case (Nat(a), Nat(b)) => 
                Nat(a + b)
            case (alpha: Nat, beta: Transfinite) =>
                beta
            case (Transfinite(e1, _, _), Transfinite(e2, _, _)) if e1 < e2 => 
                beta
            case (alpha@Transfinite(e1, c1, r1), Transfinite(e2, c2, r2)) if e1 == e2 => 
                Transfinite(e1, c1 + c2, r2)
            case (alpha@Transfinite(e1, c1, r1), beta@Nat(b)) =>
                Util.transAlwaysGreaterThanTail(alpha)
                addNat(alpha, beta)
            case (alpha@Transfinite(e1, c1, r1), beta@Transfinite(e2, c2, r2)) => 
                Equals.equivalenceOfEquals(e1, e2)
                Cmp.symetry(e1, e2)
                assert(e2 < e1)
                Util.transAlwaysGreaterThanTail(alpha)
                val omegaExp = alpha.omegaExp
                assert(r1 < omegaExp)
                assert(beta < omegaExp)
                Transfinite(e1, c1, add(r1, beta))
        }
    }

    def natMult(alpha: Nat, beta: Ordinal): Ordinal = {
        require(zero < alpha)
        beta match {
            case Transfinite(exp, coef, tail) =>
                Transfinite(exp, coef, natMult(alpha, tail))
            case Nat(b) =>
                Nat(alpha.n * b)
        }
    }.ensuring(res => res.exp == beta.exp)

    def mult(alpha: Ordinal, beta: Ordinal): Ordinal = {
        decreases(beta.length)
        import lemmas.{Mult, Cmp}

        (alpha, beta) match {
            case (Nat(BigInt(0)), _) =>
                Nat(0)
            case (_, Nat(BigInt(0))) =>
                Nat(0)
            case (a: Nat, b) =>
                natMult(a,b)
            case (Transfinite(exp, coef, tail), Nat(b)) =>
                Transfinite(exp, coef * b, tail)
            case (a: Transfinite, b: Transfinite) =>
                val newExp = add(a.exp, b.exp)
                val newCoef = b.coef
                val newTail = a * b.tail

                Mult.tailSmallerThanOmegaExp(a, b)
                assert(Nat(0) < newExp)

                Transfinite(newExp, newCoef, newTail)
        }
    }

    val omega = Transfinite(Nat(1), 1, Nat(0)) // omega^1 * 1 + 0
    val ω = omega
    val zero = Nat(0) 
    //val Zero = Nat(0) // not supported by stainless
    
    object Zero {
        def unapply(a: Ordinal): Option[Unit] = if(a == Nat(0)) Some(()) else None()
        //def unapply(a: Ordinal): Boolean = a == Nat(0) // not supported by stainless
    }
}
