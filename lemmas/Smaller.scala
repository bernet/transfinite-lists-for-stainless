package project
package lemmas

import stainless.annotation._
import stainless.lang._
import stainless.collection._
import stainless.equations._

import implementation._
import implementation.Ordinal._


object Smaller{

    def impliesAtLeastAsSmall(a: Ordinal, b: Ordinal) = {
        require(a < b)
    }.ensuring(a <= b)

    def simplifiedExp(a: Ordinal, b: Ordinal) = {
        require(a.exp < b.exp)
    }.ensuring( a < b )

    def simplified(a: Transfinite, b: Transfinite) = {
        Equals.equivalenceOfEquals(a.exp, b.exp)
    }.ensuring{
        val  expSmlr =  a.exp <  b.exp
        val coefSmlr = (a.exp == b.exp) && (a.coef <  b.coef)
        val tailSmlr = (a.exp == b.exp) && (a.coef == b.coef) && (a.tail < b.tail)

        (a < b) == (expSmlr || coefSmlr || tailSmlr)
    }

    def transitivity(alpha: Ordinal, beta: Ordinal, gamma: Ordinal): Unit = {
        require(alpha < beta && beta < gamma)
        (alpha, beta, gamma) match {
            case (_: Nat, _, _) =>
                assert(alpha < gamma)
            case (Transfinite(e1,c1,r1), Transfinite(e2,c2,r2), Transfinite(e3,c3,r3)) =>
                val  expComp12 = e1 cmp e2
                val coefComp12 = Ordinal.cmpN(c1, c2)
                val tailComp12 = r1 cmp r2

                val  expComp23 = e2 cmp e3
                val coefComp23 = Ordinal.cmpN(c2, c3)

                if (e1 < e2) {
                    if(e2 < e3){
                        transitivity(e1, e2, e3)
                        assert(alpha < gamma)
                    } else {
                        (expComp23 orElse coefComp23) match {
                            case Eq =>
                                CmpRes.lemmas.totalEqualImpliesFirstEqual(expComp23, coefComp23)
                            case Lt =>
                                CmpRes.lemmas.totalHasValueDifferentThanFirstImpliesFirstEqual(expComp23, coefComp23)
                        }
                        assert(expComp23 == Eq)
                        Equals.equivalenceOfEquals(e2, e3)
                        assert(alpha < gamma)
                    }
                } else {
                    CmpRes.lemmas.totalHasValueDifferentThanFirstImpliesFirstEqual(expComp12, coefComp12 orElse tailComp12)
                    Equals.equivalenceOfEquals(e1, e2)
                    assert(e1 == e2)

                    if (c1 == c2 && !(e2 < e3) && !(c2 < c3)) {
                        transitivity(r1, r2, r3)
                    }

                    assert(alpha < gamma)
                }
        }
        assert(alpha < gamma)
    }.ensuring(alpha < gamma)

    def widenedTransitivity(a: Ordinal, b: Ordinal, c: Ordinal) = {
        require( (a < b && b <= c) || (a <= b && b < c) )
        Equals.equivalenceOfEquals(a,b)
        Equals.equivalenceOfEquals(b,c)
        if(a == b){
            assert(b < c)
            assert(a < c)
        }else if(b == c){
            assert(a < b)
            assert(a < c)
        }else{
            assert(a < b)
            assert(b < c)
            transitivity(a, b, c)
        }
    }.ensuring(a < c)
}