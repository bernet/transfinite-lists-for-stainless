package project
package lemmas

import stainless.annotation._
import stainless.lang._
import stainless.collection._
import stainless.equations._

import implementation._
import implementation.Ordinal._


object Max{
    // ----- Max -----

    def associativity(a: Ordinal, b: Ordinal, c: Ordinal): Unit = {
        if(max(a, b) == a){
            assert(b <= a)

            if(max(b, c) == b){
                SmallerEquals.transitivity(c, b, a)
                if(max(a, c) == c){
                    Cmp.symetry(a,c)
                    assert(a <= c)
                    assert(a == c)
                }
                assert(max(a, c) == a)

                assert( max(a, max(b, c)) == max(max(a, b), c) )
            }

        }else{
            assert(max(a,b) == b)

            if(max(b, c) == c){
                SmallerEquals.transitivity(a, b, c)

                assert( max(a, max(b, c)) == max(max(a, b), c) )
            }
        }
    }.ensuring( max(a, max(b, c)) == max(max(a, b), c) )
}