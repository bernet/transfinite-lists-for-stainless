package project
package lemmas

import stainless.annotation._
import stainless.lang._
import stainless.collection._
import stainless.equations._

import implementation._
import implementation.Ordinal._


object Mult{
    // ----- Multiplication (mult, *) -----

    @inlineOnce
    def tailSmallerThanOmegaExp(a: Ordinal, b: Transfinite) = {
        require(Nat(0) < a)
        val ab @ Transfinite(_, _, _) = a * b
        //Util.omegaExpProperties(ab)
        ab
    }.ensuring(res => res.tail < res.omegaExp)

    def natByTransfiniteDoesntChangeHead(a: Nat, b: Transfinite): Ordinal = {
        require(Nat(0) < a)

        val ab @ Transfinite(_, _, _) = a * b

        b.tail match {
            case tail: Nat =>

            case tail: Transfinite =>
                natByTransfiniteDoesntChangeHead(a, tail)
                Add.headPlusTailIdentity(b)
        }

        ab
    }.ensuring(res => res.head == b.head)

    def monotonicity(k: Ordinal)(a: Ordinal, b: Ordinal): Unit = {
        require(Nat(0) < k && a < b)
        decreases(a.length, b.length)

        (k, a, b) match {
            case (_, Nat(BigInt(0)), _) =>

            case (_, _, Nat(BigInt(0))) =>

            case (_, a: Transfinite, b: Nat) =>
                assert(false)

            case (k: Nat, a: Transfinite, b: Transfinite) =>
                val la @ Transfinite(_, _, _) = k * a
                val lb @ Transfinite(_, _, _) = k * b

                Smaller.simplified(a, b)
                Smaller.simplified(la, lb)
                natByTransfiniteDoesntChangeHead(k, a)
                natByTransfiniteDoesntChangeHead(k, b)

                if (a.exp == b.exp && a.coef == b.coef) {
                    assert(a.tail < b.tail)
                    monotonicity(k)(a.tail, b.tail)
                    assert(la.exp == lb.exp)
                    assert(la.coef == lb.coef)
                    assert(la.tail < lb.tail)
                }
            case (k: Nat, _, _) =>

            case (k: Transfinite, a: Nat, b: Nat) =>
                val la @ Transfinite(_, _, _) = k * a
                val lb @ Transfinite(_, _, _) = k * b

                Smaller.simplified(la, lb)
                assert(la.exp == lb.exp)
                assert(la.coef < lb.coef)
            case (k: Transfinite, a: Nat, b: Transfinite) =>
                Add.monotonicity(k.exp)(Nat(0), b.exp)
                Add.zeroIsIdentity(k.exp)

            case (k: Transfinite, a: Transfinite, b: Transfinite) =>
                val la @ Transfinite(_, _, _) = k * a
                val lb @ Transfinite(_, _, _) = k * b

                Smaller.simplified(a, b)
                Smaller.simplified(la, lb)

                if (a.exp < b.exp) {
                    Add.monotonicity(k.exp)(a.exp, b.exp)
                    assert(la.exp < lb.exp)
                }
                else if (a.coef < b.coef) {

                }
                else {
                    assert(a.tail < b.tail)
                    monotonicity(k)(a.tail, b.tail)
                }
        }
    }.ensuring(k * a < k * b)

    def weakenedMonotonicity(k: Ordinal)(a: Ordinal, b: Ordinal): Unit = {
        require(Nat(0) < k && a <= b)
        if(a < b){
            monotonicity(k)(a, b)
        }else{
            Equals.equivalenceOfEquals(a,b)
            assert(a == b)
            Equals.impliesAtLeastAsSmall(k * a, k * b)
        }
    }.ensuring(k * a <= k * b)

    def monotonicity(a: Ordinal, b: Ordinal)(k: Ordinal): Unit = {
        require(a < b)
        decreases(k.length)

        def impl(a: Ordinal, b: Ordinal): Unit = {
            if(a < b){
                Smaller.impliesAtLeastAsSmall(a,b)
            }
            if(a == b) {
                Equals.impliesAtLeastAsSmall(a, b)
            }
        }.ensuring( ((a < b) || (a == b)) ==> (a <= b) )

        impl(a * k, b * k)

        (a, b, k) match {
            case (_, _, Nat(BigInt(0))) =>
                assert(a * k <= b * k)
            case (_, Nat(BigInt(0)), _) =>
                assert(false)
            case (Nat(BigInt(0)), _, _) =>
                assert(a * k <= b * k)
            case (a: Nat, b: Nat, k: Nat) =>
                assert(a * k <= b * k)
            case (a: Nat, b: Transfinite, k: Nat) =>
                assert(a * k <= b * k)
            case (a: Transfinite, b: Transfinite, k: Nat) =>
                assert(a * k <= b * k)
            case (a: Nat, b: Nat, k: Transfinite) =>
                val ak@Transfinite(_,_,_) = a * k
                val bk@Transfinite(_,_,_) = b * k
                monotonicity(a, b)(k.tail)
                Equals.impliesAtLeastAsSmall(ak.exp, bk.exp)
                SmallerEquals.simplified(ak, bk)
                assert(a * k <= b * k)
            case (a: Nat, b: Transfinite, k: Transfinite) =>
                val ak@Transfinite(_,_,_) = a * k
                val bk@Transfinite(_,_,_) = b * k
                monotonicity(a, b)(k.tail)
                assert(ak.head == k.head)
                Add.biggerOrEqualToInputs(b.exp, k.exp)
                Equals.impliesAtLeastAsSmall(k.head.tail, bk.head.tail)
                SmallerEquals.simplified(k.head, bk.head)
                assert(a * k <= b * k)
            case (a: Transfinite, b: Transfinite, k: Transfinite) =>
                val ak@Transfinite(_,_,_) = a * k
                val bk@Transfinite(_,_,_) = b * k
                monotonicity(a, b)(k.tail)
                assert(a.exp <= b.exp)
                Add.weakenedMonotonicity(a.exp, b.exp)(k.exp)
                SmallerEquals.simplified(ak, bk)
                assert(a * k <= b * k)
        }

    }.ensuring(a * k <= b * k)

    def associativity(a: Ordinal, b: Ordinal, c: Ordinal): Unit = {
        decreases(a.size, b.size, c.size)

        (a,b,c) match {
            case _ if a == zero || b == zero || c == zero =>
                assert(a * (b * c) == (a * b) * c)
            case (a: Nat, b: Nat, c: Nat) =>
                assert(a * (b * c) == (a * b) * c)
            case (_, _, c: Nat) =>
                assert(a * (b * c) == (a * b) * c)
            case (_, _, c: Transfinite) =>
                Add.associativity(a.exp, b.exp, c.exp)
                associativity(a,b,c.tail)
                assert(a * (b * c) == (a * b) * c)
        }
    }.ensuring( a * (b * c) == (a * b) * c )

    def oneIsIdentity(@induct a: Ordinal): Unit = {

    }.ensuring(Nat(1) * a == a && a * Nat(1) == a)

    private def _leftDistributivity(k: Transfinite)(a: Transfinite, b: Transfinite): Unit = {
        decreases(k.size, a.size, b.size, 0)

        val ka@Transfinite(_,_,_) = k * a
        val kb@Transfinite(_,_,_) = k * b

        if(a.exp < b.exp){
            Add.monotonicity(k.exp)(a.exp, b.exp)
            assert( k * (a + b) == (k * a) + (k * b) )
        }else if(a.exp == b.exp){
            Equals.equivalenceOfEquals(ka.exp, kb.exp)
            assert( k * (a + b) == (k * a) + (k * b) )
        }else{
            Equals.equivalenceOfEquals(a.exp, b.exp)
            Cmp.symetry(a.exp, b.exp)
            assert(b.exp < a.exp)
            Add.monotonicity(k.exp)(b.exp, a.exp)
            Equals.equivalenceOfEquals(ka.exp, kb.exp)
            Cmp.symetry(ka.exp, kb.exp)
            assert(b.exp < a.exp)

            leftDistributivity(k)(a.tail, b)
            
            assert( k * (a + b) == (k * a) + (k * b) )
        }

    }.ensuring( k * (a + b) == (k * a) + (k * b) )
    
    def leftDistributivity(k: Ordinal)(a: Ordinal, b: Ordinal): Unit = {
        decreases(k.size, a.size, b.size, 1)
        
        val (_k, _a, _b) = (k, a, b)

        (k, a, b) match {
            case (Nat(BigInt(0)), _, _) =>
                assert( k * (a + b) == (k * a) + (k * b) )
                assert( _k * (_a + _b) == (_k * _a) + (_k * _b) )
            case (_, Nat(BigInt(0)), _) =>
                assert( k * (a + b) == (k * a) + (k * b) )
                assert( _k * (_a + _b) == (_k * _a) + (_k * _b) )
            case (_, _, Nat(BigInt(0))) =>
                Add.zeroIsIdentity(a)
                Add.zeroIsIdentity(k * a)
                assert( k * (a + b) == (k * a) + (k * b) )
                assert( _k * (_a + _b) == (_k * _a) + (_k * _b) )
            case (k: Nat, a: Nat, b: Nat) =>
                assert( k * (a + b) == (k * a) + (k * b) )
                assert( _k * (_a + _b) == (_k * _a) + (_k * _b) )
            case (k: Nat, a: Nat, b: Transfinite) =>
                assert( k * (a + b) == (k * a) + (k * b) )
                assert( _k * (_a + _b) == (_k * _a) + (_k * _b) )
            case (k: Nat, a: Transfinite, b: Nat) =>
                leftDistributivity(k)(a.tail, b)
                assert( k * (a + b) == (k * a) + (k * b) )
                assert( _k * (_a + _b) == (_k * _a) + (_k * _b) )
            case (k: Nat, a: Transfinite, b: Transfinite) =>
                leftDistributivity(k)(a.tail, b)
                assert( k * (a + b) == (k * a) + (k * b) )
                assert( _k * (_a + _b) == (_k * _a) + (_k * _b) )
            case (k: Transfinite, a: Nat, b: Nat) =>
                val ka@Transfinite(_,_,_) = k * a
                val kb@Transfinite(_,_,_) = k * b
                Equals.equivalenceOfEquals(ka.exp, kb.exp)
                assert( k * (a + b) == (k * a) + (k * b) )
                assert( _k * (_a + _b) == (_k * _a) + (_k * _b) )
            case (k: Transfinite, a: Nat, b: Transfinite) =>
                Add.monotonicity(k.exp)(zero,b.exp)
                Add.zeroIsIdentity(k.exp)
                assert( k * (a + b) == (k * a) + (k * b) )
                assert( _k * (_a + _b) == (_k * _a) + (_k * _b) )
            case (k: Transfinite, a: Transfinite, b: Nat) =>
                val ka@Transfinite(_,_,_) = k * a
                val kb@Transfinite(_,_,_) = k * b
                
                Add.monotonicity(k.exp)(zero,a.exp)
                Add.zeroIsIdentity(k.exp)
                Util.omegaExpProperties(ka)
                Add.biggerOrEqualToInputs(k.exp, a.exp)
                assert( (k * a.tail).exp < (k.exp + a.exp) )
                assert( zero < (k.exp + a.exp) )
                Equals.equivalenceOfEquals(kb.exp, k.exp + a.exp)
                Cmp.symetry(kb.exp, k.exp + a.exp)
                
                @inline
                def o(x: Ordinal) = x //shortened type ascription since `==:|` is invariant
                
                {
                    k * (a + b)                                                ==:| trivial |:
                    k * Transfinite(a.exp, a.coef, addNat(a.tail, b))          ==:| trivial |:
                    o(Transfinite(k.exp + a.exp, a.coef, k * (a.tail + b)))    ==:| leftDistributivity(k)(a.tail, b) |:
                    //o(Transfinite(k.exp + a.exp, a.coef, (k * a.tail) + kb)) ==:| trivial |:
                    Transfinite(k.exp + a.exp, a.coef, k * a.tail) + kb        ==:| trivial |:
                    (k * a) + (k * b)
                }.qed

                assert( k * (a + b) == (k * a) + (k * b) )
                assert( _k * (_a + _b) == (_k * _a) + (_k * _b) )
            case (k: Transfinite, a: Transfinite, b: Transfinite) =>
                _leftDistributivity(k)(a,b)
                assert( k * (a + b) == (k * a) + (k * b) )
                assert( _k * (_a + _b) == (_k * _a) + (_k * _b) )
        }
        Util.TODO // Even though the match is exhaustive, and every case proves 
                  // the (unshadowed) post-condition, stainless times out
        assert( _k * (_a + _b) == (_k * _a) + (_k * _b) )
    }.ensuring( k * (a + b) == (k * a) + (k * b) )

    def zeroIsAbsorbing(a: Ordinal): Unit = {

    }.ensuring( a * zero == zero && zero * a == zero)

    def resZeroImpliesArgZero(a: Ordinal, b: Ordinal): Unit = {

    }.ensuring( ( a * b == zero ) ==> ( ( a == zero ) || ( b == zero) ) )

}