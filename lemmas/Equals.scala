package project
package lemmas

import stainless.annotation._
import stainless.lang._
import stainless.collection._
import stainless.equations._

import implementation._
import implementation.Ordinal._


object Equals{

    def impliesAtLeastAsSmall(a: Ordinal, b: Ordinal) = {
        require(a == b)
        equivalenceOfEquals(a, b)
    }.ensuring(a <= b)

    def equivalenceOfEquals(a: Ordinal, b: Ordinal): Unit = {

        def equalsImplesEqeq(a: Ordinal, b: Ordinal): Unit = {
            require(a equals b)
            (a, b) match {
                case (_: Nat, _: Nat) =>
                    assert(a == b)
                case (Transfinite(e1, c1, r1), Transfinite(e2, c2, r2)) =>
                    equalsImplesEqeq(e1, e2)
                    equalsImplesEqeq(r1, r2)
                    assert(a == b)
            }

        }.ensuring(a == b)

        def eqeqImpliesEquals(a: Ordinal, b: Ordinal): Unit = {
            require(a == b)
            (a, b) match {
                case (_: Nat, _: Nat) =>
                    assert(a equals b)
                case (Transfinite(e1, c1, r1), Transfinite(e2, c2, r2)) =>
                    eqeqImpliesEquals(e1, e2)
                    eqeqImpliesEquals(r1, r2)
                    assert(a equals b)
            }

        }.ensuring( a equals b)

        if (a equals b) equalsImplesEqeq(a, b)
        if (a == b) eqeqImpliesEquals(a, b)
    }.ensuring( (a equals b) == (a == b) )
}