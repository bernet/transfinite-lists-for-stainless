package project
package lemmas

import stainless.annotation._
import stainless.lang._
import stainless.collection._
import stainless.equations._

import implementation._
import implementation.Ordinal._


object SmallerEquals{
    
    def iffSmallerOrEqual(a: Ordinal, b: Ordinal) = {
        if(a < b){
            Smaller.impliesAtLeastAsSmall(a,b)
        }
        if(a == b){
            Equals.impliesAtLeastAsSmall(a,b)
        }
        if(a <= b){
            impliesEqualOrSmaller(a,b)
        }
    }.ensuring( ((a < b) || (a == b)) == (a <= b))

    def simplified(a: Transfinite, b: Transfinite) = {
        require((a.exp <= b.exp) && (a.coef <= b.coef) && (a.tail <= b.tail))
    }.ensuring{
        (a <= b)
    }

    def iffNotSmaller(a: Ordinal, b: Ordinal) = {
        Cmp.symetry(a, b)
    }.ensuring( !(a < b) == (b <= a) )

    def impliesEqualOrSmaller(a: Ordinal, b: Ordinal) = {
        require(a <= b)
        (a cmp b) match {
            case Eq =>
                Equals.equivalenceOfEquals(a,b)
                assert(a == b)
            case Lt =>
                assert(a < b)
            case Gt =>
                assert(false)
        }
    }.ensuring( (a == b) || (a < b) )

    
    def transitivity(alpha: Ordinal, beta: Ordinal, gamma: Ordinal) = {
        require(alpha <= beta && beta <= gamma)
        (alpha == beta, beta == gamma) match {
            case (false, false) =>
                impliesEqualOrSmaller(alpha, beta)
                impliesEqualOrSmaller(beta, gamma)
                Smaller.transitivity(alpha, beta, gamma)
                assert(alpha <= gamma)
            case _ =>
                assert(alpha <= gamma)
        }

        assert(alpha <= gamma)
    }.ensuring(alpha <= gamma)
}