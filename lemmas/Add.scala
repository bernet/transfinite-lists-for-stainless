package project
package lemmas

import stainless.annotation._
import stainless.lang._
import stainless.collection._
import stainless.equations._

import implementation._
import implementation.Ordinal._

object Add{
    // ----- Addition (add, +) -----

    @inlineOnce
    def biggerOrEqualToInputs(a: Ordinal, b: Ordinal): Ordinal = {
        decreases( a.length + b.length )

        val sum = a + b

        SmallerEquals.iffSmallerOrEqual(a, sum)
        SmallerEquals.iffSmallerOrEqual(b, sum)

        (a, b) match {
            case (Zero(_), _) =>
                assert(b == sum)
            case (_, Zero(_)) =>
                zeroIsIdentity(a)
                assert(a == sum)
            case (a: Nat, b: Nat) =>
                assert( a < sum )
                assert( b < sum )
            case (a: Nat, b: Transfinite) =>
                assert( a < sum )
                assert( b == sum )
                assert( b <= sum )
            case (a: Transfinite, b: Nat) =>
                assert( sum.isInstanceOf[Transfinite] )
                sum match {
                    case sum: Transfinite =>
                        assert( a.exp == sum.exp )
                        Equals.impliesAtLeastAsSmall(a.exp, sum.exp)
                        assert( a.coef == sum.coef )
                        assert( sum.tail == (a.tail + b) )
                        biggerOrEqualToInputs(a.tail, b)
                        assert( a.tail <= (a.tail + b) )
                        SmallerEquals.simplified(a, sum)
                        assert( a < sum )
                        assert( b < sum )
                }
            case (a: Transfinite, b: Transfinite) =>
                Cmp.symetry(a.exp, b.exp)

                sum match{
                    case sum: Transfinite => {
                        if( a.exp < b.exp ) {
                            assert( b == sum)
                            Equals.impliesAtLeastAsSmall(b, sum)
                            assert( a < sum )
                            assert( b <= sum )
                        }else if( a.exp == b.exp ) { // Transfinite(e1, c1 + c2, r2)
                            assert( a.exp == sum.exp)
                            assert( a.coef < sum.coef)
                            Smaller.simplified(a, sum)
                            Smaller.impliesAtLeastAsSmall(a, sum)
                            assert( a < sum )
                            assert( b < sum )
                        } else {
                            assert( b.exp < a.exp ) // Transfinite(e1, c1, add(r1, beta))
                            assert( a.exp == sum.exp )
                            Equals.impliesAtLeastAsSmall(a.exp, sum.exp)
                            assert( a.coef == sum.coef )
                            biggerOrEqualToInputs(a.tail, b)
                            assert( a.tail <= sum.tail )
                            SmallerEquals.simplified(a, sum)
                            assert( a < sum )
                            assert( b < sum )
                        }
                    }
                }
        }

        Cmp.symetry(a, b)
        sum
    }.ensuring(
        res =>
            a <= res &&
            b <= res &&
            (if (a == zero) b == res
            else if (b == zero) a == res
            else if (a < b) a < res && b <= res
            else a < res && b < res)
    )


    @inlineOnce
    def expEqExpMax(a: Ordinal, b: Ordinal): Unit = {
        (a, b) match {
            case (a: Nat, b: Nat) =>
            case (a: Nat, b: Transfinite) =>
            case (a: Transfinite, b: Nat) =>
            case (a: Transfinite, b: Transfinite) =>
        }
    }.ensuring( (a + b).exp == max(a.exp, b.exp) )


    def expNotSmallerThanInputsExp(a: Ordinal, b: Ordinal): Unit = {
        expEqExpMax(a,b)
    }.ensuring(a.exp <= Ordinal.add(a, b).exp && b.exp <= Ordinal.add(a, b).exp )


    /**
     * The sum of two ordinals can never exceed an ordinal that is greater than them and is of the form Omega^(something)
     * Said differently, finite sums cannot allow us to "push through" to the next power of Omega
     */
    @inlineOnce
    def neverExceedsUpperBound(a: Ordinal, b: Ordinal, bound: Transfinite): Unit = {
        require(a < bound && b < bound && bound.tail == zero && bound.coef == 1)
        a match {
            case a: Nat =>
                assert(a.exp < bound.exp)
            case a: Transfinite =>
                assert(a.exp < bound.exp)
        }
        assert(a.exp < bound.exp)

        Cmp.symetry(a, bound)
        assert(a.exp < bound.exp)
        Cmp.symetry(b, bound)
        assert(b.exp < bound.exp)

    }.ensuring(Ordinal.add(a, b) < bound)

    def headPlusTailIdentity(a: Transfinite): Unit = {
        assert(a.tail < a.head)
        val h = a.head
        val t = a.tail
        val sum@Transfinite(_, _, _) = h + t
        expEqExpMax(h, t)
        Util.headProperties(a)
        Cmp.symetry(t.exp, h.exp) //necessary because stainless cannont unfold max without (a < b) ==> !(b < a)
        assert(sum.exp == h.exp)
        assert(sum.exp == a.exp)
        assert(sum.head == h)
        assert(sum.tail == t)
    }.ensuring(a == a.head + a.tail)

    def zeroIsIdentity(@induct a: Ordinal): Unit = {

    }.ensuring(a == a + zero && a == zero + a)

    def associativity(a: Ordinal, b: Ordinal, c: Ordinal): Unit = {
        decreases(a.size)

        a match {
            case a: Nat =>
            case a@Transfinite(_, _, t: Nat) =>
                val h = a.head
                val bc = b + c

                if(h.exp < b.exp){
                    assert(h + b == b)
                    expEqExpMax(b, c)
                    Smaller.widenedTransitivity(h.exp, b.exp, bc.exp)
                    assert(h.exp < bc.exp)
                    assert(h + bc == bc)
                    assert(a + (b + c) == (a + b) + c)
                } else if(h.exp < c.exp) {
                    Cmp.symetry(b.exp, h.exp)
                    assert(b.exp <= h.exp)
                    Smaller.widenedTransitivity(b.exp, h.exp, c.exp)
                    assert(b.exp < c.exp)
                    assert(bc == c)
                    assert(h + c == c)
                    assert( (h + b) + c == c )
                    assert(a + (b + c) == (a + b) + c)
                }
            case a@Transfinite(_, _, t: Transfinite) =>
                val h = a.head

                {
                    a + (b + c)         ==:| headPlusTailIdentity(a)    |:
                    (h + t) + (b + c)   ==:| associativity(h, t, b + c) |:
                    h + (t + (b + c))   ==:| associativity(t, b, c)     |:
                    h + ((t + b) + c)   ==:| associativity(h, t + b, c) |:
                    (h + (t + b)) + c   ==:| associativity(h, t, b)     |:
                    ((h + t) + b) + c   ==:| headPlusTailIdentity(a)    |:
                    (a + b) + c 
                }.qed
        }
    }.ensuring( a + (b + c) == (a + b) + c )

    def monotonicity(k: Ordinal)(a: Ordinal, b: Ordinal): Unit = {
        require(a < b)
        decreases(k.length)

        k match {
            case _: Nat =>
                assert( k + a < k + b )
            case k: Transfinite =>
                val ka@Transfinite(_,_,_) = k + a
                val kb@Transfinite(_,_,_) = k + b
                if(k.exp < a.exp){
                    Smaller.widenedTransitivity(k.exp, a.exp, b.exp)
                    assert(k.exp < b.exp)
                    assert( k + a < k + b )
                } else if(k.exp < b.exp){
                    Cmp.symetry(k.exp, a.exp)
                    assert(a.exp <= k.exp)
                    Smaller.widenedTransitivity(a.exp, k.exp, b.exp)
                    assert(a.exp < b.exp)
                    assert( k + a < k + b )
                } else {

                    assert(ka.exp == kb.exp)
                    if(ka.coef == kb.coef){
                        assert(ka.head == kb.head)
                        assert(ka.exp == k.exp)

                        monotonicity(k.tail)(a, b)

                        assert(ka.tail < kb.tail)
                        Smaller.simplified(ka, kb)
                        assert( k + a < k + b )
                    }else{
                        Equals.equivalenceOfEquals(ka, kb)
                        assert(ka.coef < kb.coef)

                        Smaller.simplified(ka, kb)

                        assert( k + a < k + b )
                    }
                }
        }
    }.ensuring( k + a < k + b )

    def weakenedMonotonicity(k: Ordinal)(a: Ordinal, b: Ordinal): Unit = {
        require(a <= b)
        SmallerEquals.impliesEqualOrSmaller(a,b)
        if(a < b){
            monotonicity(k)(a, b)
            Smaller.impliesAtLeastAsSmall(k + a, k + b)
        } else{
            assert(a == b)
            Equals.impliesAtLeastAsSmall(k + a, k + b)
        }

    }.ensuring( k + a <= k + b )
    
    
    def monotonicity(a: Ordinal, b: Ordinal)(k: Ordinal): Unit = {
        require(a < b)
        decreases(a.length)

        val (_k, _a, _b) = (k, a, b)
        
        if(b.exp < k.exp){
            assert(b + k == k)
            Smaller.simplifiedExp(b,k)
            assert(b < k)
            assert(a.exp <= b.exp)
            Smaller.widenedTransitivity(a.exp, b.exp, k.exp)
            assert(a + k == k)
            Equals.impliesAtLeastAsSmall(a + k, b + k)
            assert(a + k <= b + k)
            assert(_a+_k <=_b +_k )
        } else {
            if (a.exp < k.exp) {
                Cmp.symetry(b.exp, k.exp)
                assert(k.exp <= b.exp)
                
                assert(a + k == k)
                
                assert(a + k <= b + k)
                assert(_a+_k <=_b +_k )
            } else {
                Cmp.symetry(b.exp, k.exp)
                assert(k.exp <= b.exp)
                Cmp.symetry(a.exp, k.exp)
                assert(k.exp <= a.exp)

                (a, b) match {
                    case (a: Nat, _) =>

                        assert(a + k <= b + k)
                        assert(_a+_k <=_b +_k )
                    case (a: Transfinite, b: Transfinite) =>
                        val la@Transfinite(_,_,_) = a + k
                        val ra@Transfinite(_,_,_) = b + k

                        headPlusTailIdentity(a)
                        associativity(a.head, a.tail, k)
                        assert(la == a.head + (a.tail + k))

                        headPlusTailIdentity(b)
                        associativity(b.head, b.tail, k)
                        assert(ra == b.head + (b.tail + k))

                        if(a.head == b.head){
                            Smaller.simplified(a, b)
                            assert(a.tail < b.tail)

                            monotonicity(a.tail, b.tail)(k)

                            assert(a.tail + k <= b.tail + k)

                            weakenedMonotonicity(a.head)(a.tail + k, b.tail + k)

                            assert(a + k <= b + k)
                            assert(_a+_k <=_b +_k )
                        }else{
                            Cmp.symetry(a.head, b.head)
                            Equals.equivalenceOfEquals(a.head, b.head)
                            assert(a.head < b.head)
                            if(a.exp < b.exp){
                                assert(la.head < ra.head)
                                assert(a + k < b + k)
                                assert(a + k <= b + k)
                                assert(_a+_k <=_b +_k )
                            }else{
                                Equals.equivalenceOfEquals(a.exp, b.exp)
                                assert(a.exp == b.exp)

                                assert(a + k <= b + k)
                                assert(_a+_k <=_b +_k )
                            }
                            assert(a + k <= b + k)
                            assert(_a+_k <=_b +_k )
                        }
                        assert(a + k <= b + k)
                        assert(_a+_k <=_b +_k )
                }
                assert(a + k <= b + k)
                assert(_a+_k <=_b +_k )
            }
            assert(a + k <= b + k)
            assert(_a+_k <=_b +_k )
        }
        assert(a + k <= b + k)
        assert(_a+_k <=_b +_k )
    }.ensuring(a + k <= b + k)

    def weakenedMonotonicity(a: Ordinal, b: Ordinal)(k: Ordinal): Unit = {
        require(a <= b)
        SmallerEquals.impliesEqualOrSmaller(a,b)
        if(a < b){
            monotonicity(a, b)(k)
        } else if (a == b){
            Equals.impliesAtLeastAsSmall(a + k, b + k)
        } else{
            assert(false)
        }

    }.ensuring( a + k <= b + k )


    @inlineOnce
    def leftCancellative(k: Ordinal)(a: Ordinal, b: Ordinal): Unit = {
        require(k + a == k + b)

        def helper(k: Ordinal, a: Ordinal, b: Ordinal): Unit = {
            require( (k + a == k + b) && a < b )

            monotonicity(k)(a, b)
            assert( k + a < k + b)

            Equals.equivalenceOfEquals( k + a, k + b)

            assert( false )

        }.ensuring(false)

        a cmp b match {
            case Lt =>
                helper(k, a, b)
            case Gt =>
                Cmp.symetry(a, b)
                helper(k, b, a)
            case Eq =>
                Equals.equivalenceOfEquals(a, b)
        }
    }.ensuring(a == b)

}