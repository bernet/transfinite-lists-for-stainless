package project
package lemmas

import stainless.annotation._
import stainless.lang._
import stainless.collection._
import stainless.equations._

import implementation._
import implementation.Ordinal._


object Cmp{
    
    def symetry(alpha: Ordinal, beta: Ordinal): Unit = {
        (alpha, beta) match {
            case (_: Nat, _) =>
                assert( (alpha cmp beta) == (beta cmp alpha).inverse )
            case (_, _: Nat) =>
                assert( (alpha cmp beta) == (beta cmp alpha).inverse )
            case (Transfinite(e1,c1,r1), Transfinite(e2,c2,r2)) =>
                symetry(e1, e2)
                symetry(r1, r2)
        }

    }.ensuring( (alpha cmp beta) == (beta cmp alpha).inverse )
}