package project
package lemmas

import stainless.annotation._
import stainless.lang._
import stainless.collection._
import stainless.equations._

import implementation._
import implementation.Ordinal._

object Util{
    
    @extern
    def TODO = ().ensuring(false)

    def transAlwaysGreaterThanTail(alpha: Transfinite): Unit = {
        assert(alpha.tail < alpha.omegaExp) //by def of trans

        if(alpha.omegaExp < alpha) {
            Smaller.transitivity(alpha.tail, alpha.omegaExp, alpha)
        }

        assert(alpha.tail < alpha)
    }.ensuring(alpha.tail < alpha)

    def omegaExpProperties(a: Transfinite): Unit = {
        val ep = a.omegaExp
        assert(a.tail < ep)
        SmallerEquals.iffNotSmaller(a, ep)
        assert(ep <= a)
    }.ensuring(a.tail.exp < a.omegaExp.exp && a.omegaExp <= a)

    def headProperties(a: Transfinite): Unit = {
        omegaExpProperties(a)
        assert(a.tail.exp < a.head.exp)
        SmallerEquals.iffNotSmaller(a, a.head)
        assert(a.head <= a)
    }.ensuring(a.tail.exp < a.head.exp && a.head <= a)


    def omegaGreaterThanAllNats(n: Nat) = {

    }.ensuring(n < omega)
}